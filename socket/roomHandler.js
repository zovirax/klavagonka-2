import * as config from "./config";
import data from "../data";
import CommentFacade from './commentFacade';

const users = new Set();
const rooms = [/*  {name: string, userCount: number} */];
const roomsMap = new Map(); //key -> room name, value -> { users: [{id: string, username: string, status: boolean}] }

const getCurrentRoomId = socket => Object.keys(socket.rooms).find(roomId => roomsMap.has(roomId));

const leaveRoom = ({roomNsp, socket, prevRoomId = ''}) => {
    const room = roomsMap.get(prevRoomId);
    const filteredUsers = room.users
        .filter(u => u.id !== socket.id);

    roomsMap.set(prevRoomId, {users: filteredUsers});
    const userCount = rooms.find(r => r.name === prevRoomId).userCount--;
    if (userCount <= 1) {
        rooms.splice(rooms.map(r => r.name).indexOf(prevRoomId), 1);
    }

    filteredUsers.forEach(user => {
        roomNsp.to(user.id).emit('ROOM_UPDATE_USERS', {users: filteredUsers, roomName: prevRoomId});
    });

    socket.leave(prevRoomId);
    roomNsp.emit('UPDATE_ROOMS', rooms);
};

const checkIfUsersReadyToPlay = users => {
    let isReadyToPlay = users.length !== 0;
    for (const user of users) {
        if (!user.status || users.length <= 1) {
            isReadyToPlay = false;
        }
    }
    return isReadyToPlay;
};

const startGame = ({roomNsp, roomName, users}) => {
    const textId = Math.floor(Math.random() * data.texts.length);
    const millisecondsToEndGame = (config.SECONDS_TIMER_BEFORE_START_GAME + config.SECONDS_FOR_GAME) * 1000;
    const sortByProgress = (u1, u2) => {
        if (u1.progress === u2.progress) {
            return u1.lastTypedCh < u2.lastTypedCh ? -1 : 1;
        }
        return u2.progress - u1.progress;
    };

    users.forEach(user => {
        setTimeout(() => {
            const topThreeUsers = roomsMap.get(roomName).users
                .slice()
                .sort(sortByProgress)
                .slice(0, 3);

            roomNsp.to(user.id).emit('FINISH_GAME', topThreeUsers);
        }, millisecondsToEndGame);

        roomNsp.to(user.id).emit('TEXT_ID', {
            seconds: config.SECONDS_TIMER_BEFORE_START_GAME,
            textId,
            secondsForGame: config.SECONDS_FOR_GAME
        });
        roomNsp.to(user.id).emit('BEFORE_START', {
            seconds: config.SECONDS_TIMER_BEFORE_START_GAME,
            users,
            roomName
        });
    });
    rooms.find(r => r.name === roomName).isGame = true;
    roomNsp.emit('UPDATE_ROOMS', rooms);
};

const onConnection = socket => {
    const username = socket.handshake.query.username;
    if (users.has(username)) {
        socket.emit('USERNAME_ALREADY_EXISTS');
    }
    users.add(username);
    socket.emit('UPDATE_ROOMS', rooms);
    return username;
};

export default io => {
    const roomNsp = io.of('/room');

    roomNsp.on('connection', socket => {
        const username = onConnection(socket);

        const commentFacade = new CommentFacade(roomNsp, socket, username);

        socket.on('CREATE_ROOM', newRoomName => {
            const isNameTaken = roomsMap.has(newRoomName);
            if (isNameTaken) {
                socket.emit('ROOM_NAME_ALREADY_EXISTS');
                return;
            }

            const prevRoomId = getCurrentRoomId(socket);
            if (prevRoomId) {
                leaveRoom({roomNsp, socket, prevRoomId});
            }

            socket.join(newRoomName, () => {
                rooms.push({name: newRoomName, userCount: 1});
                roomsMap.set(newRoomName, {users: [{id: socket.id, status: false, username, progress: 0}]});
                roomNsp.emit('UPDATE_ROOMS', rooms);
                socket.emit('JOIN_ROOM_DONE', {roomName: newRoomName, users: roomsMap.get(newRoomName).users});
                setTimeout(() => commentFacade.greet(newRoomName), 2000);
            });
        });

        socket.on('JOIN_ROOM', ({roomId, username}) => {
            const roomUsers = roomsMap.get(roomId)?.users;
            if (roomUsers?.length === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
                return;
            }

            const prevRoomId = getCurrentRoomId(socket);
            if (prevRoomId === roomId) {
                socket.emit('JOIN_ROOM_DONE', {roomName: roomId, users: roomsMap.get(roomId)});
                return;
            }

            if (prevRoomId) {
                leaveRoom({roomNsp, socket, prevRoomId});
            }

            const users = roomsMap.get(roomId).users;
            const updatedUsers = [...users, {id: socket.id, status: false, username, progress: 0}];
            roomsMap.set(roomId, {users: updatedUsers});

            socket.join(roomId, () => {
                socket.emit('JOIN_ROOM_DONE', {roomName: roomId, users: updatedUsers});
                roomNsp.to(roomId).emit('ROOM_UPDATE_USERS', {users: updatedUsers, roomName: roomId});

                rooms.forEach(r => {
                    if (r.name === roomId) {
                        r.userCount++;
                    }
                });

                roomNsp.emit('UPDATE_ROOMS', rooms);
            });
            commentFacade.greet(roomId);
        });

        socket.on('LEAVE_ROOM', roomName => {
            const room = roomsMap.get(roomName);
            if (!room) {
                return;
            }
            const {username} = room.users.find(u => u.id === socket.id);

            commentFacade.leaveRoom(roomName);
            leaveRoom({roomNsp, socket, prevRoomId: roomName});

            const usersInRoom = roomsMap.get(roomName).users;
            const isReadyToPlay = checkIfUsersReadyToPlay(usersInRoom)
            if (isReadyToPlay) {
                commentFacade.startGame(roomName, users);
                startGame({roomNsp, roomName, users: usersInRoom});
                setInterval(() => commentFacade.reportProgress(roomName, roomsMap), 3e4);
                setInterval(() => commentFacade.random(roomName), 1e4);
            }

            users.delete(username);
        });

        socket.on('READY_TOGGLE', roomName => {
            let userStatus;
            const users = roomsMap.get(roomName).users;
            const updatedUsers = users.map(u => {
                if (u.id === socket.id) {
                    userStatus = u.status;
                    return {...u, status: !userStatus};
                }
                return u;
            });

            roomsMap.set(roomName, {users: updatedUsers});
            roomNsp.to(roomName).emit('ROOM_UPDATE_USERS', {users: updatedUsers, roomName});

            const isReadyToPlay = checkIfUsersReadyToPlay(updatedUsers);

            if (isReadyToPlay) {
                commentFacade.startGame(roomName, users);
                startGame({roomNsp, roomName, users: updatedUsers});
                setInterval(() => commentFacade.reportProgress(roomName, roomsMap), 3e4);
                setInterval(() => commentFacade.random(roomName), 1e4);
            }
        });

        socket.on('CHARACTER_TYPED', ({roomName, progress}) => {
            const users = roomsMap.get(roomName).users;
            const updatedUsers = users.map(u => {
                if (u.id === socket.id) {
                    return {...u, progress, lastTypedCh: Date.now()};
                }
                return u;
            });

            if (progress === 100) {
                commentFacade.finishGame(roomName);
            }

            roomsMap.set(roomName, {users: updatedUsers});

            roomNsp.to(roomName).emit('ROOM_UPDATE_USERS', {users: updatedUsers, roomName});
        });

        socket.on('disconnecting', () => {
            const prevRoomId = getCurrentRoomId(socket);
            const room = roomsMap.get(prevRoomId);
            const {username} = room?.users.find(u => u.id === socket.id) || {};

            if (!prevRoomId || !room) {
                return;
            }

            commentFacade.leaveRoom(prevRoomId);
            leaveRoom({roomNsp, socket, prevRoomId});
            const usersInRoom = roomsMap.get(prevRoomId).users;
            const isReadyToPlay = checkIfUsersReadyToPlay(usersInRoom);

            if (isReadyToPlay) {
                commentFacade.startGame(prevRoomId, users);
                startGame({roomNsp, roomName: prevRoomId, users: usersInRoom});
                setInterval(() => commentFacade.reportProgress(prevRoomId, roomsMap), 3e4);
                setInterval(() => commentFacade.random(roomName), 1e4);
            }

            users.delete(username);
        })
    })
};
