import {createElement, addClass} from "./helper.mjs";

const username = sessionStorage.getItem('username');

export default socket => {
    socket.on('NEW_COMMENT', comment => {
        setTimeout(() => { // make sure that bot-container is rendered
            const boxContainer = document.getElementById('bot-container');
            const commentEl = createElement({tagName: 'div', className: 'comment flex-centered'})
            setTimeout(() => addClass(commentEl, 'remove'), 4000);
            commentEl.innerText = comment;
            boxContainer.append(commentEl);
        }, 0);
    });
}
