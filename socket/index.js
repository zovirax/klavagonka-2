import roomHandler from './roomHandler'

export default io => {
    roomHandler(io);
};
