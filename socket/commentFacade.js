import comments from '../comments';

// Implementation of Facade pattern
class CommentFacade {
    constructor(io, socket, username) {
        this._io = io;
        this._socket = socket;
        this._username = username;
    }

    greet(roomName) {
        const personalGreetings = comments.personalGreetings;
        const personalGreetingId = Math.floor(Math.random() * personalGreetings.length);
        const personalGreeting = personalGreetings[personalGreetingId];

        const greetings = comments.greetings;
        const greetingId = Math.floor(Math.random() * greetings.length);
        const greeting = greetings[greetingId];

        this._socket.emit('NEW_COMMENT', personalGreeting);
        this._socket.to(roomName).emit('NEW_COMMENT', `${greeting} "${this._username}"`);
    }

    finishGame(roomName) {
        const finishingComments = comments.finishGame;
        const commentId = Math.floor(Math.random() * finishingComments.length);

        this._io.to(roomName).emit('NEW_COMMENT', `${finishingComments[commentId]} ${this._username}`);
    }

    leaveRoom(roomName) {
        const leavings = comments.leavings;
        const leavingId = Math.floor(Math.random() * leavings.length);

        this._socket.to(roomName).emit('NEW_COMMENT', `${leavings[leavingId]} ${this._username}`);
    }

    startGame(roomName, users) {
        const startComments = comments.startGame;
        const commentId = Math.floor(Math.random() * startComments.length);
        let comment = startComments[commentId];
        users.forEach(({username}, idx) => {
            comment += `\n${1 + idx}. ${username}`;
        });

        this._io.to(roomName).emit('NEW_COMMENT', comment);
    }

    random(roomName) {
        const randomComments = comments.random;
        const commentId = Math.floor(Math.random() * randomComments.length);
        this._io.to(roomName).emit('NEW_COMMENT', randomComments[commentId]);
    }

    reportProgress(roomName, roomMap) {
        const users = roomMap.get(roomName).users;
        const sortByProgress = (u1, u2) => {
            if (u1.progress === u2.progress) {
                return u1.lastTypedCh < u2.lastTypedCh ? -1 : 1;
            }
            return u2.progress - u1.progress;
        };
        let comment = 'Текущий рейтинг';

        users.sort(sortByProgress)
            .slice(0, 3)
            .forEach(({username}, idx) => {
                comment += `\n${1 + idx}. ${username}`;
            });

        this._io.to(roomName).emit('NEW_COMMENT', comment);
    }
}

export default CommentFacade;
